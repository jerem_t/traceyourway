traceYourWay
============

« Trace your way » est un jeu en 2D d’exploration à la 3eme personne (en vue de dessus), dans lequel le joueur doit trouver un trésor perdu au fond d’un labyrinthe. 

Vous devez trouver le trésor et ressortir avec, avant la fin d’un compte à rebours, symboliser par le vieillissement du personnage contrôler par le joueur.

Pour vous repérer vous pourrez dessiner, de façon très simple, la carte de ce labyrinthe rempli de pièges mortels et de PNJ pas toujours digne de confiance.  

En clair vous êtes passionner par un trésor cacher prêt de chez vous et vous voulez réussir à le trouver. Evidement cela sous entend avant votre mort. Et puis-ce que « votre vie » a (je le suppose) de l’importance pour vous, vous aurez alors la possibilité d’abandonner vos recherches, définitivement, en quittant le labyrinthe.  

Dans ce jeu, le fait d’abandonner, fait pleinement parti de l’expérience de jeu, abandonner n’est pas égale à un « Game Over » ou perdre.

Malgré tout cela, garder bien à l’esprit, que le rêve de votre personnage est de trouver ce trésor. 

Mes intentions : C’est un jeu métaphorique qui parle du fait d’avoir une passion pour quelque chose et de ces répercutions dans la vie d’une personne.