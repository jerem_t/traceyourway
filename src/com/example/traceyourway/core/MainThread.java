package com.example.traceyourway.core;

import android.graphics.Canvas;

public class MainThread extends Thread {
	

	private int mMaxFps = 60;
	private int mFramePeriod = 1000 / mMaxFps; 
	private int mMaxFramesSkips = 5;
	
	private MainView mView;
	private boolean mIsRunning = true;
	private Canvas mCanvas;
	
	public MainThread(MainView view){
		mView = view;
	}
	
	public void setRunning(boolean value){
		mIsRunning = value;
	}
	
	public void run(){
		
		long beginTime;
		long timeDiff = 0;
		int sleepTime;
		int framesSkipped;
		
		while (mIsRunning) {
			mCanvas = null;
			try {
				mCanvas = mView.getHolder().lockCanvas();
				synchronized (mView.getHolder()) {
					beginTime = System.currentTimeMillis();
					framesSkipped = 0;	// resetting the frames skipped
					// update game state
					if (mCanvas == null)
						return ;
					mView.onUpdate(mCanvas, timeDiff);
					mView.onRender(mCanvas);
					// calculate how long did the cycle take
					timeDiff = System.currentTimeMillis() - beginTime;
					// calculate sleep time
					sleepTime = (int)(mFramePeriod - timeDiff);

					if (sleepTime > 0) {
						// if sleepTime > 0 we're OK
						try {
							// send the thread to sleep for a short period
							// very useful for battery saving
							Thread.sleep(sleepTime);
						} catch (InterruptedException e) {}
					}

					/*while (sleepTime < 0 && framesSkipped < mMaxFramesSkips) {
						// we need to catch up
						mView.onUpdate(mCanvas, timeDiff);  // update without rendering
						sleepTime += mFramePeriod;	// add frame period to check if in next frame
						framesSkipped++;
					}*/
				}
			} finally {
				if (mCanvas != null) {
					mView.getHolder().unlockCanvasAndPost(mCanvas);
				}
			}
		}
	}
	
	public void setMaxFps(int fps){
		mMaxFps = fps;
		mFramePeriod = 1000 / mMaxFps;
	}
	
	public void setMaxFramesSkips(int mfs){
		mMaxFramesSkips = mfs;
	}
}
