package com.example.traceyourway.core;

import java.util.Hashtable;

import com.example.traceyourway.scenes.CreditScene;
import com.example.traceyourway.scenes.GameMenuScene;
import com.example.traceyourway.scenes.GameScene;
import com.example.traceyourway.scenes.HelpScene;
import com.example.traceyourway.scenes.MainMenuScene;
import com.example.traceyourway.scenes.MapScene;
import com.example.traceyourway.scenes.PauseScene;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainView extends SurfaceView implements SurfaceHolder.Callback {

	private Hashtable<String, Scene> mScenes;
	private String mCurrentScene = "mainMenuScene";
	private MainThread mThread;
	
	public MainView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		getHolder().addCallback(this);
		setFocusable(true);
		
		mThread = new MainThread(this);
		
		mScenes = new Hashtable<String, Scene>();
		mScenes.put("gameScene", new GameScene(this));
		mScenes.put("mainMenuScene", new MainMenuScene(this));
		mScenes.put("gameMenuScene", new GameMenuScene(this));
		mScenes.put("mapScene", new MapScene(this));
		mScenes.put("creditScene", new CreditScene(this));
		mScenes.put("helpScene", new HelpScene(this));
		mScenes.put("pauseScene", new PauseScene(this));
	}

	public void onRender(Canvas canvas){
		if (mScenes.containsKey(mCurrentScene))
			mScenes.get(mCurrentScene).onRender(canvas);
	}
	
	public void onUpdate(Canvas canvas, float deltaTime){
		if (mScenes.containsKey(mCurrentScene))
			mScenes.get(mCurrentScene).onUpdate(canvas, deltaTime);
	}
	
	public void setCurrentScene(String scene){
		Log.d("VIEW", scene);
		mCurrentScene = scene;
	}
	
	public Scene getScene(String scene){
		if (mScenes.containsKey(scene))
			return mScenes.get(scene);
		return null;
	}
	
	public Scene getCurrentScene(){
		if (mScenes.containsKey(mCurrentScene))
			return mScenes.get(mCurrentScene);
		return null;
	}
	
	public boolean onTouchEvent(MotionEvent e) {
		if (mScenes.containsKey(mCurrentScene)){
			mScenes.get(mCurrentScene).getEventManager().addEvent(e.getAction(), e.getX(), e.getY());
		}
		return true;
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK ) {
	        mThread.setRunning(false);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		mThread.setRunning(true);
		mThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		mThread.setRunning(false);
		boolean retry = true;
		while (retry){
			try{
				mThread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
		
	}

}
