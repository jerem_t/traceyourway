package com.example.traceyourway.core;

import com.example.traceyourway.utils.EventManager;

import android.graphics.Canvas;


public abstract class Scene {
	
	protected MainView mView;
	protected EventManager mEventManager;
	
	public Scene(MainView view){
		mView = view;
		mEventManager = new EventManager();
	}
	
	public EventManager getEventManager(){
		return mEventManager;
	}
	
	abstract public void onRender(Canvas canvas);
	abstract public void onUpdate(Canvas canvas, float deltaTime);
}
