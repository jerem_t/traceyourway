package com.example.traceyourway.entities;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public abstract class AObject {
	
	public enum ObjectType{
		FLOOR,
		TRAP,
		WALL,
		PLAYER,
		SPAWN,
		TREASURE,
		SKELETON,
		DETECTOR_BONUS
	}
	
	protected float mX;
	protected float mY;
	protected Drawable mSprite;
	protected ObjectType mType;
	
	public ObjectType getType(){
		return mType;
	}
	
	public void setX(float x){
		mX = x;
	}
	
	public void setY(float y){
		mY = y;
	}
	
	public float getX(){
		return mX;
	}
	
	public float getY(){
		return mY;
	}
	
	public void setSprite(Drawable sprite){
		mSprite = sprite;
	}
	
	public Drawable getSprite(){
		return mSprite;
	}
	
	abstract void render(Canvas canvas);
	abstract void update(float deltaTime);
}
