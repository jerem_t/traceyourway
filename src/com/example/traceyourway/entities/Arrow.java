package com.example.traceyourway.entities;

import com.example.traceyourway.R;

import android.content.res.Resources;
import android.graphics.Canvas;

public class Arrow extends ClickableObject {

	public Direction mDirection;
	
	public Dungeon mDungeon;
	
	public enum Direction {
		UP,
		DOWN,
		LEFT,
		RIGHT
	}
	
	public Arrow(float relativeSize, Resources res, Direction dir, Dungeon dun) {
		super(relativeSize);
		
		mDirection = dir;
		mDungeon = dun;
		switch (dir) {
		case UP:
			mSprite = res.getDrawable(R.drawable.fleche_bas);
			break;
		case DOWN:
			mSprite = res.getDrawable(R.drawable.fleche_haut);
			break;
		case LEFT:
			mSprite = res.getDrawable(R.drawable.fleche_gauche);
			break;
		case RIGHT:
			mSprite = res.getDrawable(R.drawable.fleche_droite);
			break;		
		}
	}

	@Override
	public void onClick() {
		// TODO Auto-generated method stub
		switch (mDirection) {
		case UP:
			mDungeon.getPlayer().setState(Player.Direction.SOUTH);
			break;
		case DOWN:
			mDungeon.getPlayer().setState(Player.Direction.NORTH);
			break;
		case LEFT:
			mDungeon.getPlayer().setState(Player.Direction.WEST);
			break;
		case RIGHT:
			mDungeon.getPlayer().setState(Player.Direction.EST);
			break;		
		}
	}

	@Override
	public void render(Canvas canvas)
	{
		super.render(canvas);
		switch (mDirection) {
		case UP:
			mX = 0.05f * canvas.getWidth() + mSize;
			mY = canvas.getHeight() * 0.8f;
			break;
		case DOWN:
			mX = 0.05f * canvas.getWidth() + mSize;
			mY = canvas.getHeight() * 0.8f - mSize * 2;
			break;
		case LEFT:
			mX = 0.05f * canvas.getWidth();
			mY = canvas.getHeight() * 0.8f - mSize;
			break;
		case RIGHT:
			mX = 0.05f * canvas.getWidth() + 2 * mSize;
			mY = canvas.getHeight() * 0.8f - mSize;
			break;		
		}
		mSprite.setBounds((int)mX, (int)mY, (int)(mX + mSize), (int)(mY + mSize));
		mSprite.draw(canvas);
	}
	
	@Override
	void update(float deltaTime) {
		// TODO Auto-generated method stub

	}

}
