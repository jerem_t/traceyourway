package com.example.traceyourway.entities;

import android.graphics.Canvas;

public abstract class ClickableObject extends AObject {

	protected float mRelativeSize;
	protected float mSize;
	
	protected ClickableObject(float relativeSize) {
		mRelativeSize = relativeSize;
		mSize = 0;
	}
	
	public boolean hit(float x, float y) {
		return x >= mX && x <= mX + mSize && y >= mY && y <= mY + mSize;
	}
	
	abstract public void onClick();
	
	@Override
	public
	void render(Canvas canvas) {
		mSize = canvas.getHeight() * mRelativeSize;
	}

	@Override
	abstract void update(float deltaTime);

}
