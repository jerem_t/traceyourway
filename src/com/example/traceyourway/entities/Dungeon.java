package com.example.traceyourway.entities;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.example.traceyourway.R;
import com.example.traceyourway.core.MainView;
import com.example.traceyourway.entities.AObject.ObjectType;
import com.example.traceyourway.utils.ObjectFactory;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Canvas;

public class Dungeon {
	AObject[][] 		mMap = null;
	String 				mName = "";
	int 				mMapWidth = 0;
	int 				mMapHeight = 0;
	Player				mPlayer;
	Resources 			mResources;
	int					mSpawnX;
	int 				mSpawnY;
	int 				mTreasureSpawnX;
	int 				mTreasureSpawnY;
	int 				mDetectorSpawnX;
	int 				mDetectorSpawnY;

	MainView 			mView;
	
	public Dungeon(Resources res, int id, MainView view) {
		mPlayer = new Player(res, 0, 0, this);
		mResources = res;
		mView = view;
		mTreasureSpawnX = 0;
		mTreasureSpawnY = 0;
		mDetectorSpawnX = 0;
		mDetectorSpawnY = 0;
		try {
			unserialise(id);
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getMapWidth() {
		return mMapWidth;
	}
	
	public int getMapHeight() {
		return mMapHeight;
	}
	
	private String _getAttribute(XmlResourceParser data, String atr){
		for (int i = 0; i < data.getAttributeCount(); ++i){
			if (data.getAttributeName(i).equals(atr)){
				return data.getAttributeValue(i);
			}
		}
		return null;
	}
	
	public void unserialise(int id) throws XmlPullParserException, IOException{
		XmlResourceParser data = mResources.getXml(id);
		int eventType = data.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			if(eventType == XmlPullParser.START_TAG)
			{
				if (data.getName().equals("map")) {
					_initMap(data);
				}
				if (data.getName().equals("case")) {
					_addCase(data);
				}
			}
			else if(eventType == XmlPullParser.TEXT)
			{
				//Log.d("data", data.getText());
			}
			eventType = data.next();
		}
	}
	
	private void _initMap(XmlResourceParser data) {
		String result;
		
		if ((result = _getAttribute(data, "width")) != null)
			mMapWidth = Integer.valueOf(result);
		if ((result = _getAttribute(data, "height")) != null)
			mMapHeight = Integer.valueOf(result);
		if ((result = _getAttribute(data, "name")) != null)
			mName = result;
		if (mMapWidth > 0 && mMapHeight > 0){
			mMap = new AObject[mMapHeight][mMapWidth];
			for (int y = 0; y < mMapHeight; ++y){
				for (int x = 0; x < mMapWidth; ++x){
					mMap[y][x] = new Floor(mResources, x, y, this);
				}
			}
		}
	}
	
	private void _addCase(XmlResourceParser data) {
		if (mMap == null)
			return ;
		String result;
		
		int x = -1;
		int y = -1;
		String message = "Hello, traveler!";
		ObjectType type = null;
		if ((result = _getAttribute(data, "x")) != null)
			x = Integer.valueOf(result);
		if ((result = _getAttribute(data, "y")) != null)
			y = Integer.valueOf(result);
		if ((result = _getAttribute(data, "type")) != null)
			type = ObjectType.valueOf(result);
		if ((result = _getAttribute(data, "message")) != null)
			message = result;
		AObject entity = null;
		if (x == -1 || y == -1)
			return ;
		switch (type){
			case TRAP:
				entity = new Trap(mResources, x, y, this);
				break;
			case WALL:
				entity = new Wall(mResources, x, y, this);
				break;
			case TREASURE:
				entity = new Treasure(mResources, x, y, this);
				mTreasureSpawnX = x;
				mTreasureSpawnY = y;
				break;
			case SKELETON:
				entity = new Skeleton(mResources, x, y, this, message, mView.getContext().getAssets());
				break;
			case DETECTOR_BONUS:
				entity = new DetectorBonus(mResources, x, y, this);
				mDetectorSpawnX = x;
				mDetectorSpawnY = y;
				break;
			case SPAWN:
				entity = new Spawn(mResources, x, y, this);
				mSpawnX = x;
				mSpawnY = y;
				mPlayer.place(x, y);
				break;
			default:
				return ;
		}
		mMap[y][x] = entity;
	}
	
	public void render(Canvas canvas){
		if (mMap != null){
			for (int y = 0; y < mMapHeight; ++y){
				for (int x = 0; x < mMapWidth; ++x){
					mMap[y][x].render(canvas);
				}
			}				
		}
		if (mPlayer != null)
			mPlayer.render(canvas);
	}
	
	public void reset() {
		mPlayer = new Player(mResources, mSpawnX, mSpawnY, this);
		int x = mDetectorSpawnX;
		int y = mDetectorSpawnY;
		addObject(x, y, new DetectorBonus(mResources, x, y, this));
		x = mTreasureSpawnX;
		y = mTreasureSpawnY;
		addObject(x, y, new Treasure(mResources, x, y, this));
	}
	
	public Player getPlayer(){
		return mPlayer;
	}
	
	public void update(float deltaTime){
		if (mMap != null){
			for (int y = 0; y < mMapHeight; ++y){
				for (int x = 0; x < mMapWidth; ++x){
					mMap[y][x].update(deltaTime);
				}
			}				
		}
		if (mPlayer != null)
			mPlayer.update(deltaTime);
	}
	
	public AObject getObject(int x, int y){
		return mMap[y][x];
	}
	
	public void setObject(int x, int y, AObject obj){
		mMap[y][x] = obj;
	}
	
	public void addObject(int x, int y, AObject.ObjectType type) {
		mMap[y][x] = ObjectFactory.create(type, mResources, x, y, this);
	}
	
	public void addObject(int x, int y, AObject obj) {
		mMap[y][x] = obj;
	}
	
	public int 	getSpawnX(){
		return mSpawnX;
	}
	
	public int getSpawnY(){
		return mSpawnY;
	}

	public int 	getTreasureSpawnX(){
		return mTreasureSpawnX;
	}
	
	public int getTreasureSpawnY(){
		return mTreasureSpawnY;
	}
	
	public void win(){
		reset();
		mView.setCurrentScene("mainMenuScene");
	}
}
