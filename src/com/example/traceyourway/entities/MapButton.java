package com.example.traceyourway.entities;

import com.example.traceyourway.utils.EventManager.EventData;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class MapButton {

	private int mLeft;
	private int mTop;
	private int mRight;
	private int mBot;
	private Bitmap mIcon;

	public MapButton(int mId, int mLeft, int mTop, int mRight, int mBot, Bitmap icon) {
		super();
		this.mLeft = mLeft;
		this.mTop = mTop;
		this.mRight = mRight;
		this.mBot = mBot;
		this.mIcon = icon;
	}

	public MapButton update(int mId, int mLeft, int mTop, int mRight, int mBot, Bitmap icon) {
		this.mLeft = mLeft;
		this.mTop = mTop;
		this.mRight = mRight;
		this.mBot = mBot;
		this.mIcon = icon;
		return this;
	}

	public boolean check(EventData event) {
		return (event.x >= this.mLeft && event.x <= this.mRight &&
				event.y >= this.mTop && event.y <= this.mBot);
	}

	public void draw(Canvas canvas) {
		canvas.drawBitmap(mIcon, null, new Rect(mLeft, mTop, mRight, mBot), null);
	}
}
