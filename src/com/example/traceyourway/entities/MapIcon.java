package com.example.traceyourway.entities;

import com.example.traceyourway.R;
import com.example.traceyourway.core.MainView;
import com.example.traceyourway.scenes.GameScene;

import android.content.res.Resources;
import android.graphics.Canvas;

public class MapIcon extends ClickableObject {

	private MainView mView;
	private GameScene mScene;
	
	public MapIcon(Resources res, float relativeSize, MainView view, GameScene scene) {
		super(relativeSize);
		mView = view;
		mScene = scene;
		mSprite = res.getDrawable(R.drawable.map_button);
	}

	@Override
	public void onClick() {
		mScene.setIsPressed(false);
		mView.setCurrentScene("mapScene");
	}

	@Override
	public void render(Canvas canvas) {
		super.render(canvas);
		mX = canvas.getWidth() - mSize;
		mY = 0.01f * canvas.getHeight();
		mSprite.setBounds((int)mX, (int)mY, (int)(mX + mSize), (int)(mY + mSize));
		mSprite.draw(canvas);
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub

	}

}
