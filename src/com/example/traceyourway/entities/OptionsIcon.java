package com.example.traceyourway.entities;

import com.example.traceyourway.R;
import com.example.traceyourway.core.MainView;
import com.example.traceyourway.scenes.GameScene;

import android.content.res.Resources;
import android.graphics.Canvas;

public class OptionsIcon extends ClickableObject {

	private MainView mView;
	private GameScene mScene;
	
	public OptionsIcon(Resources res, float relativeSize, MainView view, GameScene scene) {
		super(relativeSize);
		
		mScene = scene;
		mView = view;
		mSprite = res.getDrawable(R.drawable.option_button);
		mX = 0;
		mY = 0;
	}

	@Override
	public void onClick() {
		mScene.setIsPressed(false);
		mView.setCurrentScene("pauseScene");
	}

	@Override
	public void render(Canvas canvas) {
		super.render(canvas);
		mX = 0.01f * canvas.getWidth();
		mY = 0.01f * canvas.getHeight();
		mSprite.setBounds((int)mX, (int)mY, (int)(mX + mSize), (int)(mY + mSize));
		mSprite.draw(canvas);
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub

	}

}
