package com.example.traceyourway.entities;

import com.example.traceyourway.R;
import com.example.traceyourway.utils.Animation;
import com.example.traceyourway.utils.Defines;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.Log;

public class Player extends AObject {

	public enum State {
		MOVE,
		DYING,
		DIED,
		IDLE,
		STOPING
	}
	
	public enum Direction {
		NORTH,
		EST,
		SOUTH,
		WEST
	}
	
	private Dungeon 	mDungeon;
	private State 		mState;
	private float   	mTargetX;
	private float   	mTargetY;
	private float   	mSaveX;
	private float   	mSaveY;
	private int     	mStep;
	private float   	mPrecision;
	private Animation 	mAnimation;
	private Direction  	mDirection;
	private boolean 	mHasTreasure;
	private boolean 	mHasDetectorBonus;
	private Resources 	mResources;
	
	public Player(Resources res, int x, int y, Dungeon dungeon) {
		mSprite = res.getDrawable(R.drawable.player);
		mX = x;
		mY = y;
		mTargetX = x;
		mTargetY = y;
		mSaveX = x;
		mSaveY = y;
		mType = ObjectType.PLAYER;
		mDungeon = dungeon;
		mStep = 0;
		mPrecision = 15;
		mState = State.IDLE;
		mDirection = Direction.SOUTH;
		mAnimation = new Animation(res, R.drawable.player, 7);
		mHasTreasure = false;
		mHasDetectorBonus = false;
		mResources = res;
	}
	
	public void place(float x, float y) {
		mX = x;
		mY = y;
		mTargetX = x;
		mTargetY = y;
		mSaveX = x;
		mSaveY = y;
	}
	
	@Override
	public void render(Canvas canvas) {	
		int size = canvas.getWidth() / (Defines.fieldOfView * 2);
		mAnimation.draw(canvas, canvas.getWidth() / 2 - size / 2, canvas.getHeight() / 2 - size / 2, size);
	}

	@Override
	public void update(float deltaTime) {
		if (mStep == 0) {
			mX = mTargetX;
			mY = mTargetY;
			mSaveX = mTargetX;
			mSaveY = mTargetY;
			if (mState == State.MOVE)
				mState = State.IDLE;
		}
		else{
			mX = mSaveX + (mTargetX - mSaveX) * (1.0f - (float)mStep / mPrecision);
			mY = mSaveY + (mTargetY - mSaveY) * (1.0f - (float)mStep / mPrecision);
			mStep -= 1;
			mState = State.MOVE;
		}
		if (mState != State.DYING && mState != State.DIED) {
			AObject hit = checkCollision(mX, mY);
			if (hit != null) {
				switch (hit.getType()) {
				case TRAP:
					mState = State.DYING;
					mAnimation.run(5, 4, 0.5f, 0);
					mAnimation.repeat(false);
					mAnimation.setCallback(new Runnable(){
						public void run(){
							mState = State.DIED;
						}
					});
					break;
				case TREASURE:
					mHasTreasure = true;
					mDungeon.setObject((int)mX, (int)mY, new Floor(mResources, (int)mX, (int)mY, mDungeon));
					mState = State.IDLE;
					break;
				case SPAWN:
					if (mHasTreasure) {
						mHasTreasure = false;
						mDungeon.win();
					}
					mState = State.IDLE;
					break;
				case DETECTOR_BONUS:
					mHasDetectorBonus = true;
					mDungeon.setObject((int)mX, (int)mY, new Floor(mResources, (int)mX, (int)mY, mDungeon));
					mState = State.IDLE;
					break;
				}
				for (int x = -1; x <= 1; ++x){
					for (int y = -1; y <= 1; ++y){
						hit = checkCollision(mX + x, mY + y);
						if (hit != null && hit.getType() == ObjectType.SKELETON) {
							if (((Skeleton)hit).getState() != Skeleton.State.SPEAKING)
								((Skeleton)hit).setState(Skeleton.State.SPEAKING);
						}
					}
				}
			}
		}
		int dir = 0;
		if (mDirection == Direction.SOUTH)
			dir = 1;
		if (mDirection == Direction.NORTH)
			dir = 2;
		if (mDirection == Direction.WEST)
			dir = 3;
		if (mDirection == Direction.EST)
			dir = 4;
		switch (mState){
		case MOVE:
		case STOPING:
			if (mAnimation.getCurrentAnim() != dir){
				mAnimation.run(dir, 4, 0.5f, 0);
				mAnimation.repeat(true);
			}
			break;
		case IDLE:
			if (mAnimation.getCurrentAnim() != 0){
				mAnimation.run(0, 1, 100, dir - 1);
				mAnimation.repeat(true);
			}
			break;
		case DIED:
		{
			if (mAnimation.getCurrentAnim() != 6){
				if (mHasTreasure) {
					int x = mDungeon.getTreasureSpawnX();
					int y = mDungeon.getTreasureSpawnY();
					mDungeon.addObject(x, y, new Treasure(mResources, x, y, mDungeon));
					mHasTreasure = false;
				}
				mAnimation.run(6, 1, 1.0f, 0);
				mAnimation.repeat(false);
				mAnimation.setCallback(new Runnable(){
					public void run(){
						_respawn();
					}
				});
			}
			break;
		}
		}
		mAnimation.update(deltaTime);
	}
	
	public boolean hasDetectorBonus() {
		return mHasDetectorBonus;
	}

	private void _respawn(){
		mState = State.IDLE;
		mDirection = Direction.SOUTH; 
		mX = mDungeon.getSpawnX();
		mY = mDungeon.getSpawnY();
		mSaveX = mDungeon.getSpawnX();
		mSaveY = mDungeon.getSpawnY();
		mTargetX = mDungeon.getSpawnX();
		mTargetY = mDungeon.getSpawnY();
	}
	
	private AObject checkCollision(float tX, float tY) {
		if (tX >= mDungeon.getMapWidth() || tX <= -1 ||
			tY >= mDungeon.getMapHeight() || tY <= -1)
			return null;
		return mDungeon.getObject((int)tX, (int)tY);
	}
	
	public void setState(Player.Direction dir) {
		if (mState == State.DIED || mState == State.DYING)
			return ;
		mState = State.MOVE;
		if (mStep != 0)
			return ;
		mDirection = dir;
		mSaveX = mX;
		mSaveY = mY;

		AObject hit;
		switch (dir){
		case NORTH:
			hit = checkCollision(mTargetX, mTargetY + 1);
			if (hit != null && hit.getType() != ObjectType.WALL){
				mTargetY += 1;
				mStep = (int)mPrecision;
			}
			break;
		case SOUTH:
			hit = checkCollision(mTargetX, mTargetY - 1);
			if (hit != null && hit.getType() != ObjectType.WALL){
				mTargetY -= 1;
				mStep = (int)mPrecision;
			}
			break;
		case WEST:
			hit = checkCollision(mTargetX + 1, mTargetY);
			if (hit != null && hit.getType() != ObjectType.WALL){
				mTargetX += 1;
				mStep = (int)mPrecision;
			}
			break;
		case EST:
			hit = checkCollision(mTargetX - 1, mTargetY);
			if (hit != null && hit.getType() != ObjectType.WALL){
				mTargetX -= 1;
				mStep = (int)mPrecision;
			}
			break;
		}
	}

}
