package com.example.traceyourway.entities;

import com.example.traceyourway.R;
import com.example.traceyourway.utils.Animation;
import com.example.traceyourway.utils.Defines;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

public class Skeleton extends AObject {

	public enum State {
		IDLE,
		SPEAKING
	}
	
	
	private State 		mState;
	private Dungeon 	mDungeon;
	private Animation 	mAnim;
	private String 		mMessage;
	private Paint 		mPaint;
	private Typeface 	mTypeFace;
	
	public Skeleton(Resources res, int x, int y, Dungeon dungeon, String message, AssetManager mgr) {
		mSprite = res.getDrawable(R.drawable.skeleton);
		mX = x;
		mY = y;
		mType = ObjectType.SKELETON;
		mDungeon = dungeon;
		mMessage = message;
		mAnim = new Animation(res, R.drawable.skeleton_anim, 1);
		mPaint = new Paint();
		mPaint.setColor(Color.WHITE);
		mPaint.setShadowLayer(5, 0, 0, Color.BLACK);
		mTypeFace = Typeface.createFromAsset(mgr, "pixel.TTF");
		mPaint.setTypeface(mTypeFace);
	}
	
	public void setState(State st) {
		mState = st;
		if (mState == State.SPEAKING) {
			mAnim.run(0, 4, 1, 0);
			mAnim.repeat(true);			
		}
	}
	
	public Skeleton.State getState() {
		return mState;
	}
	
	@Override
	void render(Canvas canvas) {
		int size = canvas.getWidth() / (Defines.fieldOfView * 2);
		int posX = (int)(canvas.getWidth() / 2 - size / 2 - (mX - mDungeon.getPlayer().getX()) * size);
		int posY = (int)(canvas.getHeight() / 2 - size / 2 - (mY - mDungeon.getPlayer().getY()) * size);
		
		if (mState == State.IDLE) {
			mSprite.setBounds(posX, posY, posX + size, posY + size);
			mSprite.draw(canvas);
		}
		else {
			mAnim.draw(canvas, posX, posY, size);
			mPaint.setTextSize(canvas.getHeight() * 0.05f);
			canvas.drawText(mMessage, canvas.getWidth() / 2 - mPaint.measureText(mMessage) / 2, canvas.getHeight() * 0.9f, mPaint);
		}
	}
	
	@Override
	void update(float deltaTime) {
		boolean bool = false;
		for (int y = -1; y <= 1; ++y){
			for (int x = -1; x <= 1; ++x){
				if ((int)mDungeon.getPlayer().getX() == (int)(mX + x) && (int)mDungeon.getPlayer().getY() == (int)(mY + y)){
					bool = true;
					break ;
				}
			}
		}
		if (bool == false)
			mState = State.IDLE;
		if (mState == State.SPEAKING)
			mAnim.update(deltaTime);
	}

}
