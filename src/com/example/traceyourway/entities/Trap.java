package com.example.traceyourway.entities;

import com.example.traceyourway.R;
import com.example.traceyourway.utils.Defines;

import android.content.res.Resources;
import android.graphics.Canvas;

public class Trap extends AObject {

	private Dungeon mDungeon;
	private Resources mResources;
	
	public Trap(Resources res, int x, int y, Dungeon dungeon){
		mResources = res;
		mSprite = res.getDrawable(R.drawable.floor);
		mX = x;
		mY = y;
		mType = ObjectType.TRAP;
		mDungeon = dungeon;
	}
	
	@Override
	void render(Canvas canvas) {
		mSprite =
			mResources.getDrawable(
				mDungeon.getPlayer().hasDetectorBonus() ?
						R.drawable.img_two : R.drawable.floor
		);
		int size = canvas.getWidth() / (Defines.fieldOfView * 2);
		int posX = (int)(canvas.getWidth() / 2 - size / 2 - (mX - mDungeon.getPlayer().getX()) * size);
		int posY = (int)(canvas.getHeight() / 2 - size / 2 - (mY - mDungeon.getPlayer().getY()) * size);
		mSprite.setBounds(posX, posY, posX + size, posY + size);
		mSprite.draw(canvas);
	}

	@Override
	void update(float deltaTime) {
		// TODO Auto-generated method stub

	}

}
