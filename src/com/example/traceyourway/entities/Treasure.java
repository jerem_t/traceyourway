package com.example.traceyourway.entities;

import com.example.traceyourway.R;
import com.example.traceyourway.utils.Defines;

import android.content.res.Resources;
import android.graphics.Canvas;

public class Treasure extends AObject {

	private Dungeon mDungeon;
	
	public Treasure(Resources res, int x, int y, Dungeon dungeon) {
		mSprite = res.getDrawable(R.drawable.treasure); // TODO use real treasure sprite
		mX = x;
		mY = y;
		mType = ObjectType.TREASURE;
		mDungeon = dungeon;
	}
	
	@Override
	void render(Canvas canvas) {
		int size = canvas.getWidth() / (Defines.fieldOfView * 2);
		int posX = (int)(canvas.getWidth() / 2 - size / 2 - (mX - mDungeon.getPlayer().getX()) * size);
		int posY = (int)(canvas.getHeight() / 2 - size / 2 - (mY - mDungeon.getPlayer().getY()) * size);
		mSprite.setBounds(posX, posY, posX + size, posY + size);
		mSprite.draw(canvas);
	}

	@Override
	void update(float deltaTime) {
		// TODO Auto-generated method stub

	}

}
