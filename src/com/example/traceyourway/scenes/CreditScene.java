package com.example.traceyourway.scenes;

import com.example.traceyourway.R;
import com.example.traceyourway.core.MainView;
import com.example.traceyourway.core.Scene;
import com.example.traceyourway.utils.EventManager.EventData;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;

public class CreditScene extends Scene {

	private int mButtonWidth;
	private int mButtonHeight;
	
	private Drawable mButton;
	
	private MainView mView;
	
	private Boolean mStatus = false;
	
	public CreditScene(MainView view) {
		super(view);
		mButton = view.getContext().getResources().getDrawable(R.drawable.menu_return);
		mButtonWidth = mButton.getIntrinsicWidth();
		mButtonHeight = mButton.getIntrinsicHeight();
		mView = view;
	}

	@Override
	public void onRender(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		mButton.setBounds(0, canvas.getHeight() - mButtonHeight, mButtonWidth, canvas.getHeight());
		mButton.draw(canvas);
	}

	public Boolean goBack(EventData e)
	{
		Rect coor = mButton.getBounds();

		if (e.x >= coor.left &&
				e.x <= coor.right &&
				e.y >= coor.top &&
				e.y <= coor.bottom && 
				e.type == MotionEvent.ACTION_DOWN)
		{
			Log.d("go back in credit scene", "credit scene");
			mView.setCurrentScene("mainMenuScene");
		}
		return (false);
	}

	@Override
	public void onUpdate(Canvas canvas, float deltaTime) {
		EventData event;
		
		while ((event = mEventManager.getEvent()) != null){
			if (goBack(event) == false) {
				break;
			}
		}
	}

}
