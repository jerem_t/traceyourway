package com.example.traceyourway.scenes;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;

import com.example.traceyourway.core.MainView;
import com.example.traceyourway.core.Scene;
import com.example.traceyourway.entities.Arrow;
import com.example.traceyourway.entities.ClickableObject;
import com.example.traceyourway.entities.Dungeon;
import com.example.traceyourway.entities.MapIcon;
import com.example.traceyourway.entities.OptionsIcon;
import com.example.traceyourway.entities.Player;
import com.example.traceyourway.utils.EventManager.EventData;

import com.example.traceyourway.R;

public class GameScene extends Scene {

	private Dungeon 				mDungeon;
	private boolean 				mIsPressed;
	private float  					mTouchX;
	private float 					mTouchY;
	private List<ClickableObject> 	mClickables;
	
	public void setIsPressed(Boolean bool) {
		mIsPressed = bool;
	}
	
	public Dungeon getDungeon()
	{
		return (mDungeon);
	}
	
	public GameScene(MainView view) {
		super(view);
		
		mDungeon = new Dungeon(((Activity) view.getContext()).getResources(), R.xml.map1, view);
		mIsPressed = false;
		mTouchX = 0;
		mTouchY = 0;
		mClickables = new ArrayList<ClickableObject>();
		mClickables.add(new OptionsIcon(((Activity) view.getContext()).getResources(), 0.15f, view, this));
		mClickables.add(new MapIcon(((Activity) view.getContext()).getResources(), 0.15f, view, this));
		mClickables.add(new Arrow(0.15f, ((Activity) view.getContext()).getResources(), Arrow.Direction.UP, mDungeon));
		mClickables.add(new Arrow(0.15f, ((Activity) view.getContext()).getResources(), Arrow.Direction.DOWN, mDungeon));
		mClickables.add(new Arrow(0.15f, ((Activity) view.getContext()).getResources(), Arrow.Direction.LEFT, mDungeon));
		mClickables.add(new Arrow(0.15f, ((Activity) view.getContext()).getResources(), Arrow.Direction.RIGHT, mDungeon));

	}

	@Override
	public void onRender(Canvas canvas) {
		// TODO Auto-generated method stub
		
		canvas.drawColor(Color.BLACK);
		mDungeon.render(canvas);
		for (int i = 0; i < mClickables.size(); ++i){
			mClickables.get(i).render(canvas);
		}
	}

	@Override
	public void onUpdate(Canvas canvas, float deltaTime) {
		EventData event;
		while ((event = mEventManager.getEvent()) != null){
			switch (event.type) {
	    	case MotionEvent.ACTION_MOVE:
	    		mTouchX = event.x;
	    		mTouchY = event.y;
	    		break;
	    	case MotionEvent.ACTION_DOWN:
	    		mIsPressed = true;
	    		mTouchX = event.x;
	    		mTouchY = event.y;
	    		break;
	    	case MotionEvent.ACTION_UP:
	    		mIsPressed = false;
	    		break;
			}
		}
		if (mIsPressed) {
			// NPCs et menus
			_handleClick(mTouchX, mTouchY, canvas);
		}

		mDungeon.update(deltaTime);
	}
	
	// Call the onClick method if an object is clicked
	private boolean _handleClick(float x, float y, Canvas canvas) {
		for (int i = 0; i < mClickables.size(); ++i) {
			if (mClickables.get(i).hit(x, y))
				mClickables.get(i).onClick();
		}
		return false;
	}
	
	private Player.Direction _getDirection(Canvas canvas) {
		float[] v1 = new float[2];
		float[] v2 = new float[2];
		float len;
		float angle;
		
		v1[0] = 1;
		v1[1] = 0;
		v2[0] = mTouchX - canvas.getWidth() / 2;
		v2[1] = (mTouchY - canvas.getHeight() / 2) * -1;
		len = (float) Math.sqrt(v2[0] * v2[0] + v2[1] * v2[1]);
		v2[0] /= len;
		v2[1] /= len;
		angle = (float) Math.atan2(v2[1], v2[0]);
		angle = (float) (angle * 180.0 / Math.PI);
		if (angle >= -45 && angle <= 45)
			return Player.Direction.EST;
		if (angle >= 45 && angle <= 135)
			return Player.Direction.NORTH;
		if (angle >= 135 || angle <= -135)
			return Player.Direction.WEST;
		if (angle >= -135 && angle <= -45)
			return Player.Direction.SOUTH;
		return Player.Direction.SOUTH;
	}

}
