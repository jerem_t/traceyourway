package com.example.traceyourway.scenes;

import com.example.traceyourway.R;
import com.example.traceyourway.core.MainView;
import com.example.traceyourway.core.Scene;
import com.example.traceyourway.utils.EventManager.EventData;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;

public class MainMenuScene extends Scene {

	private Drawable mPlayImage;
	private Drawable mCreditImage;
	private Drawable mHelpImage;
	private Drawable mTitleImage;
	
	private int mButtonWidth;
	private int mButtonHeight;
	
	private MainView mView;
	private Canvas mCanvas;
		
	public MainMenuScene(MainView view) {
		super(view);

		Context mContext = view.getContext();
		Resources res = mContext.getResources();
				
        // cache handles to our key sprites & other drawables
		mTitleImage = res.getDrawable(R.drawable.main_title);
        mPlayImage = res.getDrawable(R.drawable.menu_start);
        mCreditImage = res.getDrawable(R.drawable.menu_credits);
        mHelpImage = res.getDrawable(R.drawable.menu_help);
        
        mButtonWidth = mPlayImage.getIntrinsicWidth();
        mButtonHeight = mPlayImage.getIntrinsicHeight();
        mView = view;
	}

	@Override
	public void onRender(Canvas canvas) {
		int	x;
		int y;
		int k;

		x = canvas.getWidth() / 2 - mButtonWidth / 2;
		y = canvas.getHeight() / 2 - mButtonHeight / 2;
		k = mButtonHeight / 2;
		canvas.drawColor(Color.BLACK);

		mTitleImage.setBounds(canvas.getWidth() / 2 - mTitleImage.getIntrinsicWidth() / 2, 
				mTitleImage.getIntrinsicHeight() / 2, 
				mTitleImage.getIntrinsicWidth() + canvas.getWidth() / 2 - mTitleImage.getIntrinsicWidth() / 2, 
				mTitleImage.getIntrinsicHeight() + mTitleImage.getIntrinsicHeight() / 2);
		mPlayImage.setBounds(x, y, mButtonWidth + x, mButtonHeight + y);
		mCreditImage.setBounds(x, mButtonHeight + y + k, mButtonWidth + x, y + k + mButtonHeight * 2);
		mHelpImage.setBounds(x, y + mButtonHeight * 2 + k * 2, mButtonWidth + x, y + k * 2 + mButtonHeight * 3);

		mTitleImage.draw(canvas);
		mPlayImage.draw(canvas);
		mCreditImage.draw(canvas);
		mHelpImage.draw(canvas);
	}

	public Boolean play(EventData e)
	{
		Rect coor = mPlayImage.getBounds();
		
		if (e.x >= coor.left &&
				e.x <= coor.right &&
				e.y >= coor.top &&
				e.y <= coor.bottom && 
				e.type == MotionEvent.ACTION_DOWN)
		{
			mView.setCurrentScene("gameScene");
		}
		return (false);
	}
	
	public Boolean credit(EventData e)
	{
		Rect coor = mCreditImage.getBounds();
		
		if (e.x >= coor.left &&
				e.x <= coor.right &&
				e.y >= coor.top &&
				e.y <= coor.bottom &&
				e.type == MotionEvent.ACTION_DOWN)
		{
			Log.d("let's credit", "credit");
 			mView.setCurrentScene("creditScene");
		}
		return (false);
	}
	
	public Boolean help(EventData e)
	{
		Rect coor = mHelpImage.getBounds();

		if (e.x >= coor.left &&
				e.x <= coor.right &&
				e.y >= coor.top &&
				e.y <= coor.bottom &&
				e.type == MotionEvent.ACTION_DOWN)
		{
			Log.d("let's help", "help");
			mView.setCurrentScene("helpScene");
		}
		return (false);
	}
	
	@Override
	public void onUpdate(Canvas canvas, float deltaTime) {
		EventData event;
		
		while ((event = mEventManager.getEvent()) != null){
			if (play(event) == false && 
					help(event) == false &&
					credit(event) == false)
			{
				break;
			}
		}
	}
}
