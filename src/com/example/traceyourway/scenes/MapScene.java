package com.example.traceyourway.scenes;

import java.util.ArrayList;
import java.util.HashMap;
import com.example.traceyourway.entities.MapButton;
import com.example.traceyourway.R;
import com.example.traceyourway.scenes.GameScene;
import com.example.traceyourway.core.MainView;
import com.example.traceyourway.core.Scene;
import com.example.traceyourway.entities.AObject;
import com.example.traceyourway.utils.EventManager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

public class MapScene extends Scene {

	private AObject.ObjectType[][] mArray = null;
	private int mWidth = 0;
	private int mHeight = 0;
	private int mMapSizeX = 0;
	private int mMapSizeY = 0;
	private float mSaveX = 0;
	private float mSaveY = 0;
	private float mCurrX = 0;
	private float mCurrY = 0;
	private int mHeightPercent;
	private int mFOV = 50;
	private HashMap<StatutValues, Bitmap> mIconMap;
	private StatutValues mStatut = StatutValues.STATUT_HAND;
	private StatutValues mPrevStat = mStatut;
	private ArrayList <MapButton> mButtonList;
	
	public MapScene(MainView view) {
		super(view);
		mStatut = StatutValues.STATUT_HAND;
		Context context = view.getContext();
		
		mIconMap = new HashMap<StatutValues, Bitmap>();
		mIconMap.put(StatutValues.STATUT_PLUS, BitmapFactory.decodeResource(context.getResources(), R.drawable.zoom_plus));
		mIconMap.put(StatutValues.STATUT_MINUS, BitmapFactory.decodeResource(context.getResources(), R.drawable.zoom_moins));
		mIconMap.put(StatutValues.STATUT_PATH, BitmapFactory.decodeResource(context.getResources(), R.drawable.textures_sol_grottev2));
		mIconMap.put(StatutValues.STATUT_HAND, BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_main));
		//mIconMap.put(StatutValues.STATUT_BOULDER, BitmapFactory.decodeResource(context.getResources(), R.drawable.boulder));
		mIconMap.put(StatutValues.STATUT_SECRET, BitmapFactory.decodeResource(context.getResources(), R.drawable.secret));
		mIconMap.put(StatutValues.STATUT_WALL, BitmapFactory.decodeResource(context.getResources(), R.drawable.texture_mur_brique));
		mIconMap.put(StatutValues.STATUT_NPC, BitmapFactory.decodeResource(context.getResources(), R.drawable.npc));
		mIconMap.put(StatutValues.STATUT_TRAP, BitmapFactory.decodeResource(context.getResources(), R.drawable.trap));
		mIconMap.put(StatutValues.STATUT_START, BitmapFactory.decodeResource(context.getResources(), R.drawable.path));
		mIconMap.put(StatutValues.STATUT_TREASURE, BitmapFactory.decodeResource(context.getResources(), R.drawable.coffre_01));
		mIconMap.put(StatutValues.STATUT_EXIT, BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_croix));

		mButtonList = new ArrayList<MapButton> ();
		GameScene gameScene = (GameScene)view.getScene("gameScene");
		mMapSizeX = gameScene.getDungeon().getMapWidth() + 50;
		mMapSizeY = gameScene.getDungeon().getMapHeight() + 50;
		mArray = new AObject.ObjectType[mMapSizeY][mMapSizeX];
		for (int y = 0 ; y < mMapSizeY ; ++y) {
			for (int x = 0 ; x < mMapSizeX ; ++x) {
				mArray[y][x] = AObject.ObjectType.FLOOR;
			}
		}
		mCurrX = mMapSizeX / 2;
		mCurrY = mMapSizeY / 2;
	}

	@Override
	public void onRender(Canvas canvas) {
		Paint paint = new Paint();
		
		if (mButtonList.isEmpty())
			init(canvas);
		else
			update(canvas);
		canvas.drawColor(Color.BLACK);
		drawGrid(canvas);
		paint.setARGB(255, 155, 155, 155);
		canvas.drawRect(new Rect(0, 0, mHeightPercent * 55, mHeight), paint);
		for (int i = 0; i < mButtonList.size(); i++) {
			mButtonList.get(i).draw(canvas);
		}
	}

	@Override
	public void onUpdate(Canvas canvas, float deltaTime) {
		EventManager.EventData event;
		while ((event = mEventManager.getEvent()) != null) {
			if (event.type == MotionEvent.ACTION_DOWN)
				for (int i = 0; i < mButtonList.size(); i++) {
					if (mButtonList.get(i).check(event)) {
						mStatut = StatutValues.values()[i];
						if (mStatut == StatutValues.STATUT_PLUS && mFOV < 200) {
							mFOV += 10;
							mStatut = mPrevStat;
						}
						else if (mStatut == StatutValues.STATUT_MINUS && mFOV > 30) {
							mFOV -= 10;
							mStatut = mPrevStat;
						}
						else if (mStatut == StatutValues.STATUT_EXIT)
						{
							mView.setCurrentScene("gameScene");
						}
						else
							mPrevStat = mStatut;
				}
			}
			else if (event.type == MotionEvent.ACTION_MOVE) {
				if (mStatut == StatutValues.STATUT_HAND) {
					if (mSaveX < event.x && mCurrX - 1 >= 0)
						mCurrX -= 0.3;
					if (mSaveX > event.x && mCurrX + 1 < mMapSizeX)
						mCurrX += 0.3;
					if (mSaveY < event.y && mCurrY - 1 >= 0)
						mCurrY -= 0.3;
					if (mSaveY > event.y && mCurrY + 1 < mMapSizeY)
						mCurrY += 0.3;
				}
				else if ((mStatut != StatutValues.STATUT_MINUS || mStatut != StatutValues.STATUT_PLUS) &&
						event.x > mHeightPercent * 55)
				{
					switch (mStatut) {
						case STATUT_PATH :
							mArray[(int)(mCurrY + (event.y / mFOV))][(int)(mCurrX + ((event.x - (mHeightPercent * 60)) / mFOV))] = AObject.ObjectType.FLOOR;
							break;
						case STATUT_TRAP :
							mArray[(int)(mCurrY + (event.y / mFOV))][(int)(mCurrX + ((event.x - (mHeightPercent * 60)) / mFOV))] = AObject.ObjectType.TRAP;
							break;
						case STATUT_WALL :
							mArray[(int)(mCurrY + (event.y / mFOV))][(int)(mCurrX + ((event.x - (mHeightPercent * 60)) / mFOV))] = AObject.ObjectType.WALL;
							break;
						case STATUT_START :
							mArray[(int)(mCurrY + (event.y / mFOV))][(int)(mCurrX + ((event.x - (mHeightPercent * 60)) / mFOV))] = AObject.ObjectType.SPAWN;
							break;
						case STATUT_TREASURE :
							mArray[(int)(mCurrY + (event.y / mFOV))][(int)(mCurrX + ((event.x - (mHeightPercent * 60)) / mFOV))] = AObject.ObjectType.TREASURE;
							break;
						case STATUT_NPC :
							mArray[(int)(mCurrY + (event.y / mFOV))][(int)(mCurrX + ((event.x - (mHeightPercent * 60)) / mFOV))] = AObject.ObjectType.SKELETON;
							break;
						default:
							break;
					}
				}
				mSaveX = event.x;
				mSaveY = event.y;
			}
		}
	}

	private void drawCase(Canvas canvas, int left, int top, int right, int bot, Bitmap icon)
	{
		canvas.drawBitmap(icon, null, new Rect(left + 1, top + 1, right - 1, bot - 1), null);
	}

	private void drawGrid(Canvas canvas) {
		float x, y;
		int mapX, mapY = 0;

		for (y = mCurrY ; y < mArray.length &&
						  mapY < mHeight;
				y += 1, mapY += mFOV) {
			for (x = mCurrX, mapX = mHeightPercent * 55 + 1 ; x < mArray[(int)y].length &&
										   mapX < mWidth;
					x += 1, mapX += mFOV) {
				switch (mArray[(int)y][(int)x]) {
				case WALL :
					drawCase(canvas, mapX, mapY, mapX + mFOV, mapY + mFOV,
							mIconMap.get(StatutValues.STATUT_WALL));
					break;
				case FLOOR :
					drawCase(canvas, mapX, mapY, mapX + mFOV, mapY + mFOV,
							mIconMap.get(StatutValues.STATUT_PATH));
					break;
				case TRAP :
					drawCase(canvas, mapX, mapY, mapX + mFOV, mapY + mFOV,
							mIconMap.get(StatutValues.STATUT_TRAP));
					break;
				case PLAYER :
					drawCase(canvas, mapX, mapY, mapX + mFOV, mapY + mFOV,
							mIconMap.get(StatutValues.STATUT_NPC));
					break;
				case SPAWN :
					drawCase(canvas, mapX, mapY, mapX + mFOV, mapY + mFOV,
							mIconMap.get(StatutValues.STATUT_START));
					break;
				case TREASURE :
					drawCase(canvas, mapX, mapY, mapX + mFOV, mapY + mFOV,
							mIconMap.get(StatutValues.STATUT_TREASURE));
					break;
				case SKELETON :
					drawCase(canvas, mapX, mapY, mapX + mFOV, mapY + mFOV,
							mIconMap.get(StatutValues.STATUT_NPC));
					break;
				}
			}
		}
	}
	
	private void init (Canvas canvas) {
		mHeight = canvas.getHeight();
		mWidth = canvas.getWidth();
		mHeightPercent = mHeight / 100;

		mButtonList.add(new MapButton(0, mHeightPercent * 5, mHeightPercent * 5, mWidth - mHeightPercent * 5, mHeightPercent * 25,  mIconMap.get(StatutValues.STATUT_PATH)));
		//mButtonList.add(new MapButton(1, mHeightPercent * 5, mHeightPercent * 30, mWidth - mHeightPercent * 5, mHeightPercent * 50,  mIconMap.get(StatutValues.STATUT_BOULDER)));
		mButtonList.add(new MapButton(1, mHeightPercent * 5, mHeight - mHeightPercent * 50, mWidth - mHeightPercent * 5, mHeight - mHeightPercent * 30,  mIconMap.get(StatutValues.STATUT_TREASURE)));
		mButtonList.add(new MapButton(2, mHeightPercent * 5, mHeight - mHeightPercent * 25, mWidth - mHeightPercent * 5 , mHeight - mHeightPercent * 5, mIconMap.get(StatutValues.STATUT_WALL)));
		mButtonList.add(new MapButton(3, mHeightPercent * 0, mHeightPercent * 5, mWidth - mHeightPercent * 30 , mHeightPercent * 25, mIconMap.get(StatutValues.STATUT_HAND)));
		mButtonList.add(new MapButton(4, mHeightPercent * 25, mHeightPercent * 30, mWidth - mHeightPercent * 30 , mHeightPercent * 50,  mIconMap.get(StatutValues.STATUT_TRAP)));
		mButtonList.add(new MapButton(5, mHeightPercent * 25, mHeight - mHeightPercent * 50, mWidth - mHeightPercent * 30 , mHeight - mHeightPercent * 30,  mIconMap.get(StatutValues.STATUT_START)));
		mButtonList.add(new MapButton(6, mHeightPercent * 25, mHeight - mHeightPercent * 25, mWidth - mHeightPercent * 30 , mHeight - mHeightPercent * 5,  mIconMap.get(StatutValues.STATUT_NPC)));
		mButtonList.add(new MapButton(7, mWidth - mHeightPercent * 80, mHeight - mHeightPercent * 20, mWidth - mHeightPercent * 90, mHeight - mHeightPercent * 10, mIconMap.get(StatutValues.STATUT_PLUS)));
		mButtonList.add(new MapButton(8, mWidth - mHeightPercent * 60, mHeight - mHeightPercent * 20, mWidth - mHeightPercent * 70, mHeight - mHeightPercent * 10, mIconMap.get(StatutValues.STATUT_MINUS)));
		mButtonList.add(new MapButton(9, mWidth - mHeightPercent * 80, mHeightPercent * 10, mWidth - mHeightPercent * 90, mHeightPercent * 20, mIconMap.get(StatutValues.STATUT_EXIT)));
	}

	private void update(Canvas canvas) {
		mButtonList.set(0, mButtonList.get(0).update(0, mHeightPercent * 5 , mHeightPercent * 5,mHeightPercent * 25, mHeightPercent * 25,  mIconMap.get(StatutValues.STATUT_PATH)));
		//mButtonList.set(1, mButtonList.get(1).update(1, mHeightPercent * 5 , mHeightPercent * 30, mHeightPercent * 25, mHeightPercent * 50,  mIconMap.get(StatutValues.STATUT_BOULDER)));
		mButtonList.set(1, mButtonList.get(1).update(1, mHeightPercent * 5 , mHeight - mHeightPercent * 50, mHeightPercent * 25,mHeight - mHeightPercent * 30,  mIconMap.get(StatutValues.STATUT_TREASURE)));
		mButtonList.set(2, mButtonList.get(2).update(2, mHeightPercent * 5, mHeight - mHeightPercent * 25, mHeightPercent * 25 , mHeight - mHeightPercent * 5, mIconMap.get(StatutValues.STATUT_WALL)));
		mButtonList.set(3, mButtonList.get(3).update(3, mHeightPercent * 30, mHeightPercent * 5, mHeightPercent * 50 , mHeightPercent * 25, mIconMap.get(StatutValues.STATUT_HAND)));
		mButtonList.set(4, mButtonList.get(4).update(4, mHeightPercent * 17, mHeightPercent * 30, mHeightPercent * 37 , mHeightPercent * 50,  mIconMap.get(StatutValues.STATUT_TRAP)));
		mButtonList.set(5, mButtonList.get(5).update(5, mHeightPercent * 30, mHeight - mHeightPercent * 50, mHeightPercent * 50 , mHeight - mHeightPercent * 30,  mIconMap.get(StatutValues.STATUT_START)));
		mButtonList.set(6, mButtonList.get(6).update(6, mHeightPercent * 30, mHeight - mHeightPercent * 25, mHeightPercent * 50 , mHeight - mHeightPercent * 5,  mIconMap.get(StatutValues.STATUT_NPC)));
		mButtonList.set(7, mButtonList.get(7).update(7, mWidth - mHeightPercent * 35,mHeight - mHeightPercent * 20,mWidth - mHeightPercent * 25,mHeight - mHeightPercent * 10, mIconMap.get(StatutValues.STATUT_PLUS)));
		mButtonList.set(8, mButtonList.get(8).update(8, mWidth - mHeightPercent * 15,mHeight - mHeightPercent * 20,mWidth - mHeightPercent * 5,mHeight - mHeightPercent * 10, mIconMap.get(StatutValues.STATUT_MINUS)));
		mButtonList.set(9, mButtonList.get(9).update(9, mWidth - mHeightPercent * 15, mHeightPercent * 8, mWidth - mHeightPercent * 5, mHeightPercent * 18, mIconMap.get(StatutValues.STATUT_EXIT)));
	}
}