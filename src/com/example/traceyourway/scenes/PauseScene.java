package com.example.traceyourway.scenes;

import com.example.traceyourway.R;
import com.example.traceyourway.core.MainView;
import com.example.traceyourway.core.Scene;
import com.example.traceyourway.utils.EventManager.EventData;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;

public class PauseScene extends Scene {

	private Drawable mResumeImage;
	private Drawable mRetryImage;
	private Drawable mHelpImage;
	private Drawable mMainMenuImage;
	private Drawable mTitleImage;
	
	private int mButtonWidth;
	private int mButtonHeight;
	
	private MainView mView;
		
	public PauseScene(MainView view) {
		super(view);

		Context mContext = view.getContext();
		Resources res = mContext.getResources();
				
        // cache handles to our key sprites & other drawables
		mTitleImage = res.getDrawable(R.drawable.main_title);
        mResumeImage = res.getDrawable(R.drawable.menu_start);
        mRetryImage = res.getDrawable(R.drawable.menu_start);
        mHelpImage = res.getDrawable(R.drawable.menu_start);
        mMainMenuImage = res.getDrawable(R.drawable.menu_start);
        
        mButtonWidth = mResumeImage.getIntrinsicWidth();
        mButtonHeight = mResumeImage.getIntrinsicHeight();
        mView = view;
	}

	@Override
	public void onRender(Canvas canvas) {
		int	x;
		int y;
		int k;

		x = canvas.getWidth() / 2 - mButtonWidth / 2;
		y = canvas.getHeight() / 2 - mButtonHeight / 2;
		k = mButtonHeight / 2;
		canvas.drawColor(Color.BLACK);

		mTitleImage.setBounds(canvas.getWidth() / 2 - mTitleImage.getIntrinsicWidth() / 2, 
				mTitleImage.getIntrinsicHeight() / 2, 
				mTitleImage.getIntrinsicWidth() + canvas.getWidth() / 2 - mTitleImage.getIntrinsicWidth() / 2, 
				mTitleImage.getIntrinsicHeight() + mTitleImage.getIntrinsicHeight() / 2);
		mResumeImage.setBounds(x, y, mButtonWidth + x, mButtonHeight + y);
		mRetryImage.setBounds(x, mButtonHeight + y + k, mButtonWidth + x, y + k + mButtonHeight * 2);
		mHelpImage.setBounds(x, y + mButtonHeight * 2 + k * 2, mButtonWidth + x, y + k * 2 + mButtonHeight * 3);
		mMainMenuImage.setBounds(x, y + mButtonHeight * 3 + k * 3, mButtonWidth + x, y + k * 3 + mButtonHeight * 4);

		mTitleImage.draw(canvas);
		mResumeImage.draw(canvas);
		mRetryImage.draw(canvas);
		mHelpImage.draw(canvas);
		mMainMenuImage.draw(canvas);
	}

	public Boolean resume(EventData e)
	{
		Rect coor = mResumeImage.getBounds();
		
		if (e.x >= coor.left &&
				e.x <= coor.right &&
				e.y >= coor.top &&
				e.y <= coor.bottom && 
				e.type == MotionEvent.ACTION_DOWN)
		{
			mView.setCurrentScene("gameScene");
		}
		return (false);
	}
	
	public Boolean retry(EventData e)
	{
		Rect coor = mRetryImage.getBounds();
		
		if (e.x >= coor.left &&
				e.x <= coor.right &&
				e.y >= coor.top &&
				e.y <= coor.bottom &&
				e.type == MotionEvent.ACTION_DOWN)
		{
			Log.d("let's credit", "credit");
 			mView.setCurrentScene("creditScene");
		}
		return (false);
	}
	
	public Boolean help(EventData e)
	{
		Rect coor = mHelpImage.getBounds();

		if (e.x >= coor.left &&
				e.x <= coor.right &&
				e.y >= coor.top &&
				e.y <= coor.bottom &&
				e.type == MotionEvent.ACTION_DOWN)
		{
			Log.d("let's help", "help");
			mView.setCurrentScene("helpScene");
		}
		return (false);
	}
	
	public Boolean mainMenu(EventData e)
	{
		Rect coor = mMainMenuImage.getBounds();
		
		if (e.x >= coor.left &&
				e.x <= coor.right &&
				e.y >= coor.top &&
				e.y <= coor.bottom &&
				e.type == MotionEvent.ACTION_DOWN)
		{
			Log.d("let's quit", "quit");
			mView.setCurrentScene("mainMenuScene");
		}
		return (false);
	}
	
	@Override
	public void onUpdate(Canvas canvas, float deltaTime) {
		EventData event;
		
		while ((event = mEventManager.getEvent()) != null){
			if (retry(event) == false && 
					help(event) == false &&
					resume(event) == false &&
					mainMenu(event) == false)
			{
				break;
			}
		}
	}
}
