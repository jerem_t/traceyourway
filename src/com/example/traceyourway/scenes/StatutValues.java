package com.example.traceyourway.scenes;

public enum StatutValues {
	STATUT_PATH,
    //STATUT_BOULDER,
    STATUT_TREASURE,
    STATUT_WALL,
    STATUT_HAND,
    STATUT_TRAP,
    STATUT_START,
    STATUT_NPC,
	STATUT_PLUS,
	STATUT_MINUS,
	STATUT_EXIT,
	STATUT_SECRET
}
