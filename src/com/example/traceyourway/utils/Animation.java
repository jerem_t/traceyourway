package com.example.traceyourway.utils;

import java.util.concurrent.Callable;

import com.example.traceyourway.entities.AObject;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

public class Animation {
	private Bitmap 	mTileSet;
	private int 	mNbAnims;
	private int 	mCurrentAnim;
	private int 	mNbSprites;
	private float   mAnimTime;	
	private float	mCurrentTime;
	private int 	mCurrentSprite;
	private RectF	mDestRect;
	private Rect    mSourceRect;
	private boolean mLoop;
	private Runnable mCallback = null;

	public Animation(Resources res, int fileId, int nbAnims) {
		mTileSet = BitmapFactory.decodeResource(res, fileId);
		mNbAnims = nbAnims;
		mCurrentAnim = 0;
		mNbSprites = 0;
		mAnimTime = 0f;
		mCurrentSprite = 0;
		mLoop = false;
	}
	
	public void run(int currentAnim, int nbSprites, float animTime, int currentSprite) {
		mCurrentAnim = currentAnim;
		mNbSprites = nbSprites;
		mAnimTime = animTime * 1000;
		mCurrentSprite = currentSprite;
		mCurrentTime = 0;
		mCallback = null;
	}
	
	public void update(float deltaTime) {
		mCurrentTime += deltaTime;
		//Log.d("ANIM", String.valueOf(mCurrentTime));
		if (mNbSprites == 1 && mCurrentTime >= mAnimTime){
			if (mCallback == null)
				return ;
			try {
				mCallback.run();
			} catch (Exception e) {
			}
			mCallback = null;
			return ;
		}
		else if (mNbSprites == 1 && mCallback == null){
			return ;
		}
		else if (mCurrentTime >= mAnimTime && mLoop == true){
			mCurrentTime = mCurrentTime - mAnimTime;
		}
		else if (mCurrentTime >= mAnimTime){
			mCurrentSprite = 0;
			if (mCallback == null)
				return ;
			mCallback.run();
			mCallback = null;
			return ;
		}
		mCurrentSprite = (int)(mCurrentTime / mAnimTime * mNbSprites);
	}
	
	public void draw(Canvas canvas, int x, int y, int size) {
        int spriteHeight = mTileSet.getHeight() / mNbAnims;
        int spriteWidth = spriteHeight;
        mSourceRect = new Rect(mCurrentSprite * spriteWidth, mCurrentAnim * spriteHeight,
        			mCurrentSprite * spriteWidth + spriteWidth, mCurrentAnim * spriteHeight + spriteHeight);
        mDestRect = new RectF(x, y, x + size, y + size);
		if (mSourceRect == null || mDestRect == null)
			return ;
        canvas.drawBitmap(mTileSet, mSourceRect, mDestRect, null);
	}
	
	public void repeat(boolean loop) {
		mLoop = loop;
	}
	
	public int getCurrentAnim(){
		return mCurrentAnim;
	}
	
	public int getCurrentSprite(){
		return mCurrentSprite;
	}

	public void setCallback(Runnable func) {
		mCallback = func;
	}
}
