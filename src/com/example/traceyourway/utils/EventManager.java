package com.example.traceyourway.utils;

import java.util.ArrayList;
import java.util.List;


public class EventManager {

	public class EventData {
		public float x;
		public float y;
		public int type;
	}
	
	private ArrayList<EventData> mEventList;
	
	public EventManager(){
		mEventList = new ArrayList<EventData>();
	}
	
	public void addEvent(int type, float x, float y){
		EventData data = new EventData();
		data.x = x;
		data.y = y;
		data.type = type;
		mEventList.add(data);
	}
	
	public EventData getEvent(){
		if (mEventList.size() > 0){
			EventData data = mEventList.get(0);
			mEventList.remove(0);
			return data;
		}
		return null;
	}

}
