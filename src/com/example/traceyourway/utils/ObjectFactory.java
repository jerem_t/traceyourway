package com.example.traceyourway.utils;

import android.content.res.Resources;

import com.example.traceyourway.entities.AObject;
import com.example.traceyourway.entities.Dungeon;
import com.example.traceyourway.entities.Floor;
import com.example.traceyourway.entities.Skeleton;
import com.example.traceyourway.entities.Spawn;
import com.example.traceyourway.entities.Trap;
import com.example.traceyourway.entities.Treasure;
import com.example.traceyourway.entities.Wall;

public class ObjectFactory {
	static public AObject create(AObject.ObjectType objType, Resources res, int x, int y, Dungeon dgn) {
		switch (objType) {
		case FLOOR:
			return new Floor(res, x, y, dgn);
		case WALL:
			return new Wall(res, x, y, dgn);
		case SPAWN:
			return new Spawn(res, x, y, dgn);
		case TRAP:
			return new Trap(res, x, y, dgn);
		case TREASURE:
			return new Treasure(res, x, y, dgn);
		}
		return null;
	}
}
